package com.ics.DorinatorApp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.*;
import com.ics.dorinator.kryo.*;

import java.util.ArrayList;
import java.util.List;

/*
 * Class: DorinatorActivity
 * Developer: balogh
 * Last edited: 2015.06.07. 20:33
 */

public class DorinatorActivity extends Activity {
    public static final List<String> list = new ArrayList<>();
    public static ProgressDialog loadingDialog;
    private static ArrayAdapter<String> dataAdapter;

    public static void updateList() {
        dataAdapter.notifyDataSetChanged();
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String name = parent.getItemAtPosition(position).toString();
                Toast.makeText(parent.getContext(), "Selected user: " + name, Toast.LENGTH_SHORT).show();

                for (User user : DorinatorCommandClient.users) {
                    if (user.name.equals(name)) DorinatorCommandClient.activeUser = user;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        AlertDialog.Builder webpageAlert = new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Send & Show Webpage")
                .setMessage("Webpage URL")
                .setView(input)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        WebpageRequest request = new WebpageRequest();
                        request.url = input.getText().toString();
                        request.tIndex = DorinatorCommandClient.activeUser.index;
                        DorinatorCommandClient.send(request);
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                });
        AlertDialog webpageDialog = webpageAlert.create();

        AlertDialog.Builder shutdownAlert = new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Shutdown system")
                .setMessage("Are you sure?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ShutdownRequest request = new ShutdownRequest();
                        request.tIndex = DorinatorCommandClient.activeUser.index;
                        DorinatorCommandClient.send(request);
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                });
        AlertDialog shutdownDialog = shutdownAlert.create();

        AlertDialog.Builder shutdownClientAlert = new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Shutdown remote client")
                .setMessage("Are you sure?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        TerminateRequest request = new TerminateRequest();
                        request.tIndex = DorinatorCommandClient.activeUser.index;
                        DorinatorCommandClient.send(request);
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                });
        AlertDialog shutdownClientDialog = shutdownClientAlert.create();

        AlertDialog.Builder destroyAlert = new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Shutdown remote client")
                .setMessage("Are you sure? Warning: PERMANENT operation")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        DestroyRequest request = new DestroyRequest();
                        request.tIndex = DorinatorCommandClient.activeUser.index;
                        DorinatorCommandClient.send(request);
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                });
        AlertDialog destroyDialog = destroyAlert.create();

        final EditText dialogInput = new EditText(this);
        dialogInput.setInputType(InputType.TYPE_CLASS_TEXT);
        AlertDialog.Builder dialogAlert = new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Show text")
                .setMessage("Text to show:")
                .setView(dialogInput)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        DialogRequest request = new DialogRequest();
                        request.text = dialogInput.getText().toString();
                        request.tIndex = DorinatorCommandClient.activeUser.index;
                        DorinatorCommandClient.send(request);
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialogDialog = dialogAlert.create();

        // Send & Show btn
        Button btn0 = (Button) findViewById(R.id.button0);
        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webpageDialog.show();
            }
        });

        // Shut down pc btn
        Button btn1 = (Button) findViewById(R.id.button1);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shutdownDialog.show();
            }
        });

        // Shut down client btn
        Button btn2 = (Button) findViewById(R.id.button2);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shutdownClientDialog.show();
            }
        });

        // Destroy client btn
        Button btn3 = (Button) findViewById(R.id.button3);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                destroyDialog.show();
            }
        });

        Button btn4 = (Button) findViewById(R.id.button4);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogDialog.show();
            }
        });

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                if (DorinatorCommandClient.client.isConnected()) {
                    MessagePacket m = new MessagePacket();
                    m.message = e.getMessage();
                    DorinatorCommandClient.send(m);
                }
            }
        });

        showLoadingDialog();
        DorinatorActivity app = this;
        new Thread(new Runnable() {
            @Override
            public void run() {
                new DorinatorCommandClient(app);
                while (true) {
                    try {
                        Thread.sleep(50000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public void showLoadingDialog() {
        loadingDialog = ProgressDialog.show(this, "Please wait", "Connecting to the server", true);
    }
}