package com.ics.DorinatorApp;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.ics.dorinator.kryo.ConnectionPacket;
import com.ics.dorinator.kryo.Kryonet;
import com.ics.dorinator.kryo.User;
import com.ics.dorinator.kryo.UserInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/*
 * Class: DorinatorCommandClient
 * Developer: balogh
 * Last edited: 2015.06.07. 20:33
 */

public class DorinatorCommandClient {
    public static final List<User> users = new ArrayList<>();
    public static User activeUser;
    public static Client client;
    private DorinatorActivity app = null;

    DorinatorCommandClient(DorinatorActivity app) {
        this.app = app;

        client = new Client(2097152, 2048);
        Kryonet.register(client);
        client.start();
        connect();
    }

    public static void send(Object o) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                client.sendTCP(o);
            }
        }).start();
    }

    private void connect() {
        try {
            client.connect(10000, "infinitecocaine.noip.me", 3050, 3050);
        } catch (IOException e) {
            while (!client.isConnected()) {
                try {
                    client.reconnect(10000);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                if (DorinatorActivity.loadingDialog != null && DorinatorActivity.loadingDialog.isShowing())
                    DorinatorActivity.loadingDialog.dismiss();
            }
        });

        sendConnectionInfo();
        listen();
    }

    private void listen() {
        client.addListener(new Listener() {
            @Override
            public void received(Connection connection, Object o) {

                /** User info */
                if (o instanceof UserInfo) {
                    User user = new User(
                            ((UserInfo) o).index, 0,
                            ((UserInfo) o).name,
                            ((UserInfo) o).osName,
                            ((UserInfo) o).osVersion,
                            ((UserInfo) o).JREVersion);

                    if (((UserInfo) o).remove) {
                        for (User user_ : users) {
                            if (user_.index == user.index) {
                                users.remove(user_);

                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    public void run() {
                                        Toast.makeText(app, user_.name + " disconnected", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }
                    } else {
                        users.add(user);
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            public void run() {
                                Toast.makeText(app, user.name + " connected", Toast.LENGTH_SHORT).show();
                            }
                        });

                        int i = 0;
                        for (User user_ : users) {
                            if (user_.index == user.index) i++;
                        }
                        if (i > 1) users.remove(user);
                    }

                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public void run() {
                            DorinatorActivity.list.clear();
                            for (User user : users) {
                                DorinatorActivity.list.add(user.name);
                            }
                            DorinatorActivity.updateList();
                        }
                    });
                }
            }

            /** Connected */
            @Override
            public void connected(Connection connection) {
                sendConnectionInfo();
                System.out.println("CONNECTED");
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        if (DorinatorActivity.loadingDialog != null && DorinatorActivity.loadingDialog.isShowing())
                            DorinatorActivity.loadingDialog.dismiss();
                    }
                });
            }

            /** Disconnected */
            @Override
            public void disconnected(Connection connection) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        DorinatorActivity.list.clear();
                        DorinatorActivity.updateList();
                        app.showLoadingDialog();
                    }
                });

                new Thread() {
                    public void run() {
                        while (!client.isConnected()) {
                            try {
                                client.reconnect(10000);
                            } catch (IOException ignore) {
                            }
                        }
                    }
                }.start();
            }
        });
    }

    private void sendConnectionInfo() {
        ConnectionPacket connectionPacket = new ConnectionPacket();
        connectionPacket.username = getUsername();
        connectionPacket.osName = System.getProperty("os.name");
        connectionPacket.osVersion = System.getProperty("os.version");
        connectionPacket.JREVersion = System.getProperty("java.version");
        connectionPacket.connectionType = 1;
        client.sendTCP(connectionPacket);
    }

    public String getUsername() {
        AccountManager manager = AccountManager.get(app);
        Account[] accounts = manager.getAccountsByType("com.google");
        List<String> possibleEmails = new LinkedList<>();

        for (Account account : accounts) {
            // TODO: Check possibleEmail against an email regex or treat
            // account.name as an email address only for certain account.type values.
            possibleEmails.add(account.name);
        }

        if (!possibleEmails.isEmpty() && possibleEmails.get(0) != null) {
            String email = possibleEmails.get(0);
            String[] parts = email.split("@");

            if (parts.length > 1)
                return parts[0];
        }
        return null;
    }
}