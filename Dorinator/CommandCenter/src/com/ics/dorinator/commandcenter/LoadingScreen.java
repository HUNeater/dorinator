package com.ics.dorinator.commandcenter;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

/*
 * Class: LoadingScreen
 * Developer: balogh
 * Last edited: 2015.05.18. 15:42
 */

class LoadingScreen extends JPanel implements Runnable {
    private final String message;
    private long lastLoopTime = System.nanoTime();
    private double lastFpsTime;
    private BufferedImage loadingImage;
    private double rotation = 0;

    public LoadingScreen(String message) {
        this.message = message;

        InputStream in = getClass().getResourceAsStream("loadingImage.png");
        try {
            loadingImage = ImageIO.read(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        AffineTransform transform = new AffineTransform();
        transform.translate(getWidth() / 2, getHeight() / 2);
        transform.rotate(Math.toRadians(rotation));
        transform.translate(-loadingImage.getWidth() / 2, -loadingImage.getHeight() / 2);

        FontMetrics metrics = g2.getFontMetrics();
        int stringWidth = metrics.stringWidth(message);

        g2.drawImage(loadingImage, transform, null);
        g2.drawString(message, getWidth() / 2 - stringWidth / 2, getHeight() / 2 + loadingImage.getHeight() / 2 + 10);
        g2.dispose();
    }

    @Override
    public void run() {
        while (DorinatorServer.users.size() <= 0) {
            long now = System.nanoTime();
            long updateLength = now - lastLoopTime;
            lastLoopTime = now;
            int FPS = 30;
            int OPTIMAL_TIME = 1000000000 / FPS;
            double delta = updateLength / (double) OPTIMAL_TIME;

            lastFpsTime += updateLength;

            if (lastFpsTime >= 1000000000) {
                lastFpsTime = 0;
            }

            update(delta);
            repaint();

            try {
                if (((lastLoopTime - System.nanoTime() + OPTIMAL_TIME) / 1000000) > 0) {
                    Thread.sleep((lastLoopTime - System.nanoTime() + OPTIMAL_TIME) / 1000000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void update(double delta) {
        rotation += 10 * delta;
    }
}