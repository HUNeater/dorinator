package com.ics.dorinator.commandcenter;

import com.ics.dorinator.kryo.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/*
 * Class: CommandCenter
 * Developer: balogh
 * Last edited: 2015.06.06. 15:21
 */

class CommandCenter {
    private static final String s = System.getProperty("file.separator");
    private static final String root = System.getProperty("user.home");
    public static JPanel content;

    public static void main(String[] args) {
        CommandCenter commandCenter = new CommandCenter();
        commandCenter.start();
    }

    public static void writeLogFile(User user) {
        File logDir = new File(root + s + "Desktop" + s + "Dorinator Logs");
        if (!logDir.exists()) logDir.mkdir();

        File logFile = new File(root + s + "Desktop" + s + "Dorinator Logs" + s + user.name + "_log.txt");
        int number = 0;
        if (logFile.exists()) {
            while (logFile.exists()) {
                number++;
                logFile = new File(root + s + "Desktop" + s + "Dorinator Logs" + s + user.name + "_log"
                        + number + ".txt");
            }
        }

        try {
            logFile.createNewFile();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        String currentTime = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.GERMAN)
                .format(Calendar.getInstance().getTime());

        String[] logLines = null;
        String[] sLogLines = null;
        try {
            if (user.log != null)
                logLines = user.log.split("\n");

            if (MenuScreen.log.getText() != null)
                sLogLines = MenuScreen.log.getText().split("\n");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            PrintStream writer = new PrintStream(logFile);
            writer.println(currentTime.substring(0, 4) + "." + currentTime.substring(4, 6) + "."
                    + currentTime.substring(6, 8) + " | " + currentTime.substring(9, 11) + ":"
                    + currentTime.substring(11, 13));
            writer.println("\n--------- Key Log ---------\n");

            if (logLines != null) {
                for (String logLine : logLines) {
                    writer.println(logLine);
                }
            }

            writer.println("\n--------- Server Log ---------\n");

            if (sLogLines != null) {
                for (String logLine : sLogLines) {
                    writer.println(logLine);
                }
            }

            writer.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    private void start() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException
                | UnsupportedLookAndFeelException
                | IllegalAccessException
                | InstantiationException e) {
            e.printStackTrace();
        }

        ImageIcon icon = new ImageIcon(getClass().getResource("/com/ics/dorinator/commandcenter/icon.png"));

        content = new JPanel(new CardLayout());
        content.add(new LoadingScreen("Waiting for incoming connection"));

        JFrame frame = new JFrame("Dorinator Command Center");
        frame.setIconImage(icon.getImage());
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.add(content);
        frame.pack();
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        frame.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {
                DorinatorServer.users.forEach(com.ics.dorinator.commandcenter.CommandCenter::writeLogFile);
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });

        new DorinatorServer();
    }
}