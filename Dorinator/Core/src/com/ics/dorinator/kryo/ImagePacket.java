package com.ics.dorinator.kryo;

/*
 * Class: ImagePacket
 * Developer: balogh
 * Last edited: 2014.09.11. 19:13
 */

public class ImagePacket {
    public byte[] bytes;
}