package com.ics.dorinator.kryo;

/*
 * Class: WebpageRequest
 * Developer: balogh
 * Last edited: 2015.06.06. 16:56
 */

public class WebpageRequest {
    public String url;
    public int tIndex;
}