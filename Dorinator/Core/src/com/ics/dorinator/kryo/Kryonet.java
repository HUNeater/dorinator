package com.ics.dorinator.kryo;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.EndPoint;

/*
 * Class: Kryonet
 * Developer: balogh
 * Last edited: 2015.06.07. 20:01
 */

public class Kryonet {
    public static void register(EndPoint endPoint) {
        Kryo kryo = endPoint.getKryo();
        kryo.register(ConnectionPacket.class);
        kryo.register(WebpageRequest.class);
        kryo.register(ScreenshotRequest.class);
        kryo.register(ImagePacket.class);
        kryo.register(MessagePacket.class);
        kryo.register(TerminateRequest.class);
        kryo.register(DestroyRequest.class);
        kryo.register(KeyPacket.class);
        kryo.register(byte[].class);
        kryo.register(UserInfo.class);
        kryo.register(ShutdownRequest.class);
        kryo.register(DialogRequest.class);
    }
}