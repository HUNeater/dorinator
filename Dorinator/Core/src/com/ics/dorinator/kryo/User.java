package com.ics.dorinator.kryo;

/*
 * Class: User
 * Developer: balogh
 * Last edited: 2015.06.06. 14:15
 */

public class User {
    public final int index;
    public final int connectionType;
    public final String osName;
    public final String osVersion;
    public final String JREVersion;
    public String name;
    public String log;

    public User(int index, int connectionType, String name, String osName, String osVersion, String JREVersion) {
        this.index = index;
        this.connectionType = connectionType;
        this.name = name;
        this.osName = osName;
        this.osVersion = osVersion;
        this.JREVersion = JREVersion;
    }
}