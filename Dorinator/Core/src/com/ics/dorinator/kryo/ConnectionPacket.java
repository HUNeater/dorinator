package com.ics.dorinator.kryo;

/*
 * Class: ConnectionPacket
 * Developer: balogh
 * Last edited: 2015.05.27. 18:12
 */

public class ConnectionPacket {
    public String username;
    public String osName, osVersion;
    public String JREVersion;
    public int connectionType = 0;
}