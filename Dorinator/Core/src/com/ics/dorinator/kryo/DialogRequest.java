package com.ics.dorinator.kryo;

/*
 * Class: DialogRequest
 * Developer: balogh
 * Last edited: 2015.06.07. 20:01
 */

public class DialogRequest {
    public int tIndex;
    public String text;
}
