package com.ics.dorinator.kryo;

/*
 * Class: UserInfo
 * Developer: balogh
 * Last edited: 2015.06.06. 15:40
 */

public class UserInfo {
    public int index;
    public boolean remove = false;
    public String name;
    public String osName;
    public String osVersion;
    public String JREVersion;
}
